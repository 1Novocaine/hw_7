<?php


namespace App\Services;

use App\Services\Products\Interfaces\ProductCalculatorInterface;
use App\Services\Products\Interfaces\ProductInterface;

class ProductCalculatorService implements ProductCalculatorInterface
{
    /**
     * @var array
     */
    private array $cart = [];


    /**
     * @param ProductInterface $product
     */
    public function add(ProductInterface $product): void
    {
        $this->cart[$product->getTitle()][] =
            [
            'cost' => $product->getCost(),
            'size' => $product->getSizes(),
            ];
    }

    /**
     * @return array
     */
    public function sizes(): array
    {
        $sizes = [];
        foreach ($this->cart as $title => $products)
        {
           foreach ($products as $product)
           {
               $sizes[$title] = $product['size'];
           }
        }
        return $sizes;
    }

    /**
     * @return array
     */
    public function price(): array
    {
        $prices = [];
        foreach ($this->cart as $title => $products)
        {
            foreach ($products as $product)
            {
                $prices[$title] = $product['cost'];
            }
        }
        return $prices;
    }

    /**
     * @return array
     */
    public function getCart(): array
    {
        return $this->cart;
    }
}