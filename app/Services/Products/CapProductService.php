<?php


namespace App\Services\Products;


class CapProductService extends AbstractProduct
{
    /**
     *  @var string
     */
    public const TITLE = 'cap';

    /**
     * @var array|int[]
     */
    protected array $sizes = [20, 21, 22, 23];
    /**
     * @var float|int
     */
    protected float $price = 10;
    /**
     * @var int
     */
    protected int $minItemToOrder = 10;
}