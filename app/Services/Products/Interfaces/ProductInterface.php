<?php


namespace App\Services\Products\Interfaces;


interface  ProductInterface
{
    /**
     * @return float
     */
    public function getCost(): float;

    /**
     * @return array
     */
    public function getSizes(): array;

    /**
     * @return string
     */
    public function getTitle(): string;

}