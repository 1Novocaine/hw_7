<?php


namespace App\Services\Products\Interfaces;


interface ProductCalculatorInterface
{
    /**
     * @param ProductInterface $product
     */
    public function add(ProductInterface $product): void;

    /**
     * @return array
     */
    public function sizes(): array;

    /**
     * @return array
     */
    public function price(): array;
}