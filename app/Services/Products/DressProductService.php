<?php


namespace App\Services\Products;


class DressProductService extends AbstractProduct
{
    /**
     *  @var string
     */
    public const TITLE = 'dress';

    /**
     * @var array|int[]
     */
    protected array $sizes = [30, 31, 32, 33, 34, 35];
    /**
     * @var float|int
     */
    protected float $price = 100;
    /**
     * @var int
     */
    protected int $minItemToOrder = 1;
}