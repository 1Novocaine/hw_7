<?php


namespace App\Services\Products;

use App\Services\Products\Interfaces\ProductInterface;

class AbstractProduct implements \App\Services\Products\Interfaces\ProductInterface
{
    /**
     * @return float
     */
    public function getCost(): float
    {
        return count($this->sizes) * $this->price * $this->getMinToOrder();
    }

    /**
     * @return array
     */
    public function getSizes(): array
    {
        return $this->sizes;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return static::TITLE;
    }

    /**
     * @return int
     */
    public function getMinToOrder (): int
    {
        return $this->minItemToOrder;
    }

    /**
     * @param int $minItemToOrder
     */
    public function setMinToOrder (int $minItemToOrder): void
    {
        $this->minItemToOrder = $minItemToOrder;
    }

}