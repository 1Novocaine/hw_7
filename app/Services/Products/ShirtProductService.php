<?php


namespace App\Services\Products;

class ShirtProductService extends AbstractProduct
{
    /**
     * @var string
     */
    public const TITLE = 'shirt';

    /**
     * @var array|int[]
     */
    protected array $sizes = [40, 41, 42];
    /**
     * @var float|int
     */
    protected float $price = 50;
    /**
     * @var int
     */
    protected int $minItemToOrder = 3;
}